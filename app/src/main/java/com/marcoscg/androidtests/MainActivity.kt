package com.marcoscg.androidtests

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.preference.PreferenceManager
import kotlinx.android.synthetic.main.activity_main.edit
import kotlinx.android.synthetic.main.activity_main.save

class MainActivity : AppCompatActivity() {

    var prefs: SharedPreferences? = null
    val validator = EmailValidator()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        prefs = PreferenceManager.getDefaultSharedPreferences(this)

        edit.addTextChangedListener(validator)

        edit.setText(prefs!!.getString("email", ""))

        save.setOnClickListener {
            saveInSharedPrefs()
        }
    }

    fun saveInSharedPrefs() {
        if (validator.isValid) {
            prefs!!.edit().putString("email", edit.text.toString()).apply()

            finish()
            startActivity(Intent(this, MainActivity::class.java))
        } else edit.error = "Invalid email"
    }
}
