# Android Tests

An android demo project that demonstrates how to integrate GitLab CI to perform unit and instrumental tests in Android.

## 🔗 Contents

 1. [Enable pipeline in your repository](#enable-pipeline-in-your-repository).
 2. [Configure pipeline](#configure-pipeline).
 3. [Useful links](#useful-links).



### Enable pipeline in your repository

To enable pipelines in your repository, navigate to the main GitLab repo page and click on "CI/CD configuration" button. A new file called "_[.gitlab-ci.yml](https://gitlab.com/marcoscgdev/android-tests/blob/master/.gitlab-ci.yml)_" will be added to the repo. This file is everything you need to configure the pipeline.



### Configure pipeline

We are going to use the default java 8 jdk image. First of all, we need to define some android project related variables in the config file (to access them easily). We need to define the android compile sdk, android build tools, android sdk tools version number (you can find it [here](https://developer.android.com/studio/#command-tools)) and the emulator sdk version*.

> NOTE: At this moment, only SDK <= 24 are allowed for emulators (arm-v7a). Newer SDK versions will be available soon.

---

In the **before_script** part we are downloading all the necessary android sdk tools and build tools. We are also accepting their licenses.

In the **stages** part we are adding all the script stages. In this case, only a test stage. You can add a build stage, a upload stage, etc.



#### Let's start testing!

- **Unit tests**

  For unit testing in android, we only have to execute the following command: ```./gradlew test```. NOTE: We only want this script to run on master commits.

- **Instrumental tests**

  This step is less simple. NOTE: We only want this script to run on master commits.

  First of all we are downloading two online bash scripts: The first one is to know when the android emulator is active and the second one is to kill all active android emulators.

  Then, we create the android emulator which will include google_apis. You can replace "*google_apis_playstore*" with "*default*" if you do not want it. After that, we start the emulator and the *android-wait-for-emulator* script. 

  When the emulator is active, we unlock it by "pressing" the menu button (keyevent #82, see all keyevents [here](https://gist.github.com/ctrl-freak/9abb5aea0d89d7bd9df6a3d0ac08b73c)). Then, we execute the android instrumental test command ```./gradlew connectedCheck```. Finally, we kill the emulator.

  

### Useful links

- https://about.gitlab.com/2018/10/24/setting-up-gitlab-ci-for-android-projects/
- https://about.gitlab.com/2017/11/20/working-with-yaml-gitlab-ci-android/
- https://about.gitlab.com/2019/01/28/android-publishing-with-gitlab-and-fastlane/
- https://gist.github.com/illuzor/988385c493d3f7ed7193a6e3ce001a68
- https://gist.github.com/anonymous/614aafb2d8710865c688684a8657a141
- https://gist.github.com/DennisAlund/4bfb8fa9aec068770c6d9cd92772c627